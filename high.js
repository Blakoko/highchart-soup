function AddBubble(chart) {
    var points = chart.series[0].data;
    var r = chart.renderer,

    //Positions
    TopX = chart.plotLeft - (chart.plotLeft / 2),
    TopY = chart.plotTop,
    startX = (chart.plotSizeX + chart.plotLeft),
    startY = (chart.plotSizeY + chart.plotTop) + (TopY / 2),
    HeightArrow = 5, //Hauteur Point Fleche
    WidthTip = 2; //Epaisseur Fleche

    //Texte Fleches
    TextHor = 'Potentiel',
    TextVert = 'Performance',

    //Style
    ColorArrow = '#0070c0',
    ColorTxt = 'black',
    SizeTxt = '13px',
    StyleTxt = 'italic';

    SizeArrow = (HeightArrow) + (WidthTip / 2), //Faire un triangle Equilateral
    WidthArrowLg = chart.plotSizeX - SizeArrow, // hauteur Fleche Horizontal
    WidthArrowVert = chart.plotSizeY - SizeArrow, // Longueur Fleche Vertical

    $.each(points, function (i, p) {
        if (p.value.substring(0, 1) == '*') {
            chart.addSeries({
                name: 'Position',
                type: 'bubble',
                color: 'violet',
                data: [[p.x, p.y, 0]]
            });
        }
        if (p.x == 2 && p.y == 0) {
            p.update({
                color: 'rgba(219, 157, 195, 1)'
            }, false);

        } else if (p.x < 2 && p.y > 0 && (p.x !== p.y)) {
            p.update({
                color: 'rgba(239, 239, 239, 1)'
            }, false);

        } else if (p.x > 0 && (p.y + 1) == p.x) {
            p.update({
                color: 'rgba(211, 236, 185, 1)'
            }, false);
        } else if (p.x == p.y) {
            p.update({
                color: 'rgba(255, 230, 153, 1)'
            }, false);
        }
    });

    LgHor = r.path(['M', startX, startY, 'L', startX - SizeArrow, startY - SizeArrow, 'v', HeightArrow, 'h', -WidthArrowLg, 'v', WidthTip, 'h', WidthArrowLg, 'v', HeightArrow, 'Z'])
        .attr({
            zIndex: 9999,
            fill: ColorArrow,
            //stroke: 'green',
            'stroke-width': 1
        })
        .add();

    LgVert = r.path(['M', TopX, TopY, 'L', TopX - SizeArrow, TopY + SizeArrow, 'h', HeightArrow, 'v', WidthArrowVert, 'h', WidthTip, 'v', -WidthArrowVert, 'h', HeightArrow, 'Z'])
        .attr({
            zIndex: 9999,
            fill: ColorArrow,
            //stroke: 'green',
            'stroke-width': 1
        })
        .add();

    //Recuperer la taille du Texte
    function get_tex_size(txt, font) {
        this.element = document.createElement('canvas');
        this.context = this.element.getContext("2d");
        this.context.font = font;
        var tsize = {
            'width': this.context.measureText(txt).width,
            'height': parseInt(this.context.font)
        };
        return tsize;
    }

    var HorSize = get_tex_size(TextHor, SizeTxt + ' ' + StyleTxt);
    var VertSize = get_tex_size(TextVert, SizeTxt + ' ' + StyleTxt);

    TxtHor = r.text(TextHor, startX - (HorSize['width']) - (chart.plotLeft / 2), startY + (HorSize['height']))
        .attr({
            rotation: 0,
            zIndex: 9999
        })
        .css({
            color: ColorTxt,
            fontSize: SizeTxt,
            fontStyle: StyleTxt
        })
        .add();

    TxtVert = r.text(TextVert, TopX - (VertSize['height']), TopY + (VertSize['width']) + (chart.plotTop / 2))
        .attr({
            rotation: 270,
            zIndex: 9999
        })
        .css({
            color: ColorTxt,
            fontSize: SizeTxt,
            fontStyle: StyleTxt
        })
        .add();

    chart.isDirty = true;
    chart.redraw(false);

    console.log(chart, 'test');

}
